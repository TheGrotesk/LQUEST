<?php


	namespace LERRORS;

	class SyntaxError extends \Exception{

		function  __construct($message, $code = null)
		{
			parent::__construct($message, $code);
		}
	}

?>
