<?php

	/*
	 *	Author: Alexandr Poberezhnyi 
	 */
	
	require('LERRORS/SyntaxError.php');
	
	$config = [];

	$FUNCTION_KEYS = [
		'TO', 'FROM'
	];

	$TYPE_KEYS = [
		'POST', 'GET', 'PUT', 'DELETE'
	];

	$CONTENT_TYPES_KEYS = [
		'JSON', 'FORM', 'RAW'
	];

	$OPERATION_KEYS = [
		'HEADERS',  'THAN' 
	];

	$END_KEYS = [
		'END'
	];

	$START_KEYS = [
		'{', '}'
	];

	$OPERATOR_KEYS = [
		'+', '*', '/', '-'
	];

	$ALL_KEYS = [
		'TO', 'FROM', 'JSON', 'FORM', 'RAW', 'POST', 'GET', '{', '}',
	       	'THAN', 'HEADERS', 'END'
	];
	
	if(!empty($argv[1]) && file_exists($argv[1])){
		$content = file($argv[1]);
		$check = LQUEST::check($content, $ALL_KEYS);
		$semantic = LQUEST::parse($check);
		$analizer = LQUEST::analizer($semantic);

		if(isset($argv[2]) && $argv[2] == '--show-semantic'){
			print_r($semantic);
			print_r($analizer);
		}
	}

	class LQUEST{
		
		/**
		 * Check for correct syntax in lines
		 * @param array
		 * @return array
		 * */
		public static function check(array $content, array $rules) : array
		{
			foreach($content as $k => $line){
				$line = preg_split('/[\s,\t]+/', $line);
				foreach($line as $key){
					$word = str_split($key);
					$w = null;
					foreach($word as $char){
						$w = $w.$char;
						if(in_array($w, $rules)){
							$words[] = $w;
						}elseif($w == ">"){
							$words[] = $key;
						}elseif($w == "'"){
							$words[] = $key;
						}
					}
				}
			}

			return $words;
		}
		
		/**
		 * Returns semantic three of LQUEST file
		 * @param array
		 * @return array
		 **/
		public static function parse(array $words) : array
		{
			global $FUNCTION_KEYS;
			global $TYPE_KEYS;
			global $START_KEYS;
			global $OPERATOR_KEYS;
			global $CONTENT_TYPES_KEYS;
			global $OPERATION_KEYS;
			global $END_KEYS;

			foreach($words as $k => $word){
				if(in_array($word, $FUNCTION_KEYS)){
					$semantic[] = "func@$word";
				}elseif(in_array($word, $TYPE_KEYS)){
					$semantic[] = "type@$word";
				}elseif(in_array($word, $CONTENT_TYPES_KEYS)){
					$semantic[] = "content_type@$word";
				}elseif(in_array($word, $OPERATION_KEYS)){
					$semantic[] = "operation@$word";
					$index = $k + 1;
					while(!in_array($words[$index], $END_KEYS)){
						$semantic[] = "option@$words[$index]";
						$index++;
					}
				}elseif(in_array($word, $START_KEYS)){
					if($word == $START_KEYS[0]){
						$semantic[] = "start@";
					}elseif($word == $START_KEYS[1]){
						$semantic[] = "end@";
					}
				}elseif(in_array($word, $OPERATOR_KEYS)){
					$semantic[] = "operand@$word";
				}elseif(strpos($word, ">", 0) !== false){
					$word = explode('>', $word);
					$semantic[] = "link@$word[1]";
				}elseif(strpos($word, "'", 0) !== false){
					continue;
				}elseif(in_array($word, $END_KEYS)){
					continue;	
				}else{
					throw new LERRORS\SyntaxError("Undefined key $word on line $k");
				}
			}	
			
			
			return $semantic;
		}
		
		/**
		 * Analize semantic tree and make logic
		 * @param array
		 * @return array
		 **/
		public static function analizer(array $semantic) : array
		{
			$i = 0;
			foreach($semantic as $k => $com){
				$com = explode('@', $com);
				switch($com[0]){
					case 'func':
						$count = $k;
						switch($com[1]){
							case 'TO':
								$command[$count]['class'] = 'LALGO\TO';
								break;
							case 'FROM':
								$command[$count]['class'] = 'LALGO\FROM';
								break;
						}
						break;
					case 'link':
						$command[$count]['link'] = $com[1];
						break;
					case 'type':
						$command[$count]['type'] = $com[1];
						break;
					case 'start':
						$index = $k+1;
						while($semantic[$index] !== 'end@'){
							$string = explode('@', $semantic[$index]);
								$command[$count]['body'][$i] = $semantic[$index];
							$i++;
							$index++;
						}
					 	break;
				}
			}	

			return $command;
		}	
	}
?>
